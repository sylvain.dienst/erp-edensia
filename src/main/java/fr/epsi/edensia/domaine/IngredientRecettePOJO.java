package fr.epsi.edensia.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "recette_ingredient")
public class IngredientRecettePOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String quantite;

	private RecettePOJO recette;
	private IngredientPOJO ingredient;

	/**
	 * 
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ingredient_recette")
	public Integer getId() {
		return super.getId();
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "quantite")
	public String getQuantite() {
		return quantite;
	}

	/**
	 * 
	 * @param quantite
	 */
	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "oid_recette")
	public RecettePOJO getRecette() {
		return recette;
	}

	/**
	 * 
	 * @param recette
	 */
	public void setRecette(RecettePOJO recette) {
		this.recette = recette;
	}

	/**
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "oid_ingredient")
	public IngredientPOJO getIngredient() {
		return ingredient;
	}

	/**
	 * 
	 * @param ingredient
	 */
	public void setIngredient(IngredientPOJO ingredient) {
		this.ingredient = ingredient;
	}

}
