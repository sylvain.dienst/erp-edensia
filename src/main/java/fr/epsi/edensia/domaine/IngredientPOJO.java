package fr.epsi.edensia.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ingredient")
public class IngredientPOJO extends AbstractObjetCreaMAJ {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String libelle;

	/**
	 * 
	 */
	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ingredient")
	public Integer getId() {
		return super.getId();
	}

	/**
	 * 
	 * @return
	 */
	@Column(name = "libelle")
	public String getLibelle() {
		return libelle;
	}

	/**
	 * 
	 * @param libelle
	 */
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
