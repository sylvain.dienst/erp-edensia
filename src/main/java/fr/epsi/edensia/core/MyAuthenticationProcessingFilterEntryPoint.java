package fr.epsi.edensia.core;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

public class MyAuthenticationProcessingFilterEntryPoint extends
		LoginUrlAuthenticationEntryPoint {

	private String urlFilter = null;

	@Autowired
	private AuthenticationManager authenticationManager;

	/**
	 * @param loginFormUrl
	 *            String
	 */
	public MyAuthenticationProcessingFilterEntryPoint(final String loginFormUrl) {
		super(loginFormUrl);
	}

	/**
	 * @return String getUrlFilter
	 */
	public String getUrlFilter() {
		return urlFilter;
	}

	/**
	 * @param urlFilter
	 *            String
	 */
	public void setUrlFilter(final String urlFilter) {
		this.urlFilter = urlFilter;
	}

	@Override
	public void commence(final HttpServletRequest request,
			final HttpServletResponse response,
			final AuthenticationException authException) throws IOException,
			ServletException {

		// gerer pour passer en headers plutot security !!!
		String username = request.getParameter("j_username");
		String password = request.getParameter("j_password");

		String acces = request.getParameter("acces");

		// String user = request.getHeader("j_username");
		// String password = request.getHeader("j_password");

		if (username != null && password != null) {

			if (acces != null) {
				username += acces;
			}

			final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
					username, password);

			try {
				if (authRequest != null) {
					final Authentication authentication = authenticationManager
							.authenticate(authRequest);
					final SecurityContext securityContext = SecurityContextHolder
							.getContext();
					securityContext.setAuthentication(authentication);

					String params = "";
					if (request.getQueryString() != null
							&& !request.getQueryString().isEmpty()) {
						params = "?" + request.getQueryString();
					}
					// faut pas faire mieux ????
					response.sendRedirect(request.getRequestURI() + params); // request.getContextPath()
					// +

					return;
				}
			} catch (final AuthenticationException e) {
				System.out.println("erreur authentication");
			}

		}

		final String redirectURLAjax = response.encodeRedirectURL(request
				.getContextPath() + "/jsf/commun/login.xhtml?expired=true");

		final boolean ajaxRedirect = request.getHeader("faces-request") != null
				&& request.getHeader("faces-request").toLowerCase()
						.indexOf("ajax") > -1;
		if (ajaxRedirect) {
			final Authentication authentication = SecurityContextHolder
					.getContext().getAuthentication();
			if (authentication == null) {

				final StringBuilder sb = new StringBuilder();
				sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
						.append("<partial-response><redirect url=\"")
						.append(redirectURLAjax)
						.append("\"></redirect></partial-response>");
				response.setHeader("Cache-Control", "no-cache");
				response.setCharacterEncoding("UTF-8");
				response.setContentType("text/xml");

				final PrintWriter pw = response.getWriter();
				pw.println(sb.toString());
				pw.flush();

			}
		} else {

			super.commence(request, response, authException);
		}
	}
}
