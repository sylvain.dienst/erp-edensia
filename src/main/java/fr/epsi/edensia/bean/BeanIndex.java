package fr.epsi.edensia.bean;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.facade.api.ICommunFacade;

@Component("beanIndex")
@Scope("view")
@Lazy(true)
public class BeanIndex extends AbstractGenericBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(BeanIndex.class);

	@Autowired
	private ICommunFacade communFacade;

	private List<RecettePOJO> listeRecette;

	private RecettePOJO selectedRecette;

	/**
	 * 
	 */
	@PostConstruct
	public void init() {
		this.listeRecette = this.communFacade.findAllRecette();
	}

	/**
	 * 
	 * @return
	 */
	public List<RecettePOJO> getListeRecette() {
		return listeRecette;
	}

	/**
	 * 
	 * @return
	 */
	public RecettePOJO getSelectedRecette() {
		return selectedRecette;
	}

	/**
	 * 
	 * @param selectedRecette
	 */
	public void setSelectedRecette(RecettePOJO selectedRecette) {
		this.selectedRecette = selectedRecette;
	}

	/**
	 * @param event SelectEvent
	 */
	public void onRowSelect(final SelectEvent event) {

		this.selectedRecette = ((RecettePOJO) event.getObject());

	}

	/**
	 * @param event SelectEvent
	 */
	public void onRowUnselect(final UnselectEvent event) {

		this.selectedRecette = null;

	}

}
