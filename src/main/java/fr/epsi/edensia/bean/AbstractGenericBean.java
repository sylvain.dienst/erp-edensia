/**
 * 
 */

package fr.epsi.edensia.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;

/**
 * @author mgrau
 */
public abstract class AbstractGenericBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// DEBUT INJECTION.

	// FIN INJECTION.

	private boolean modificationGeneral = false;

	public boolean isModificationGeneral() {
		return modificationGeneral;
	}

	public void setModificationGeneral(boolean modificationGeneral) {
		this.modificationGeneral = modificationGeneral;
	}

	// modification infos joueur
	/**
	* 
	*/
	public void startModification() {
		this.modificationGeneral = true;
	}

	/**
	* 
	*/
	public void endModification() {
		this.modificationGeneral = false;
	}

	/**
	 * @return String getLibelleHeader
	 */
	public String getLibelleHeader() {

		return this.getLibelleHeader("", "");

	}

	/**
	 * @param name    String
	 * @param libelle String
	 * @return String getLibelleHeader
	 */
	public String getLibelleHeader(final String name, final String libelle) {

		if (libelle == null || libelle.trim().isEmpty()) {
			return "Cr�ation " + name;
		} else if (isModificationGeneral()) {
			return "Modification " + name + " : " + libelle;
		} else {
			return "Visualisation " + name + " : " + libelle;
		}

	}

	/**
	 * @return String styleClass
	 */
	public String getClassFieldSet() {

		if (this.isModificationGeneral()) {
			return "fieldSet_Modify";
		}
		return "";
	}

	protected void resetInputValues(final List<UIComponent> children) {
		for (UIComponent component : children) {
			if (component.getChildCount() > 0) {
				resetInputValues(component.getChildren());
			} else {
				if (component instanceof EditableValueHolder) {
					EditableValueHolder input = (EditableValueHolder) component;
					input.resetValue();
				}
			}
		}
	}

}
