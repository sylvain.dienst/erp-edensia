package fr.epsi.edensia.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.CloseEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import fr.epsi.edensia.core.GetParameterUrl;
import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;
import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.dto.FeedBackExceptionDTO;
import fr.epsi.edensia.exception.ValidationException;
import fr.epsi.edensia.facade.api.ICommunFacade;

@Component("beanRecette")
@Scope("view")
@Lazy(true)
public class BeanRecette extends AbstractGenericBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(BeanRecette.class);

	@Autowired
	private ICommunFacade communFacade;

	private RecettePOJO recette;

	@GetParameterUrl(paramName = "idRecette")
	private Integer idRecette;

	private List<IngredientRecettePOJO> listeIngredientsRecette = new ArrayList<IngredientRecettePOJO>();

	private IngredientRecettePOJO ig = new IngredientRecettePOJO();

	private List<IngredientPOJO> listeIngredients;

	private IngredientPOJO selectedIngredient;

	@PostConstruct
	public void init() {
		this.listeIngredients = this.communFacade.findAllIngredient();

		if (idRecette != null) {
			this.recette = this.communFacade.getRecette(this.idRecette);
			this.listeIngredientsRecette = this.communFacade.findIngredientByRecette(this.idRecette);
		} else {
			this.recette = new RecettePOJO();
			setModificationGeneral(true);
		}
	}

	public RecettePOJO getRecette() {
		return recette;
	}

	public void setRecette(RecettePOJO recette) {
		this.recette = recette;
	}

	public List<IngredientRecettePOJO> getListeIngredientsRecette() {
		return listeIngredientsRecette;
	}

	public IngredientRecettePOJO getIg() {
		return ig;
	}

	public void setIg(IngredientRecettePOJO ig) {
		this.ig = ig;
	}

	public IngredientPOJO getSelectedIngredient() {
		return selectedIngredient;
	}

	public void setSelectedIngredient(IngredientPOJO selectedIngredient) {
		this.selectedIngredient = selectedIngredient;
	}

	public void save() {

		try {

			this.communFacade.saveOrUpdateRecette(this.recette);

			setModificationGeneral(false);
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Sauvegarde effectu�e de la recette", " "));

		} catch (final Exception e) {

			// message d erreur
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), " "));
		}

	}

	public List<IngredientPOJO> getListeIngredients() {
		return listeIngredients;
	}

	/**
	 * 
	 */
	public void annuler() {

		setModificationGeneral(false);
		this.recette = this.communFacade.getRecette(this.recette.getId());

	}

	public String delete() {
		try {

			this.communFacade.removeRecette(this.recette);

			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Suppression de la recette ", " "));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

		} catch (ValidationException ve) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ve.getMessage(), " "));

			return null;
		}

		return "ACCUEIL";

	}

	@Override
	public String getLibelleHeader() {
		return super.getLibelleHeader("Recette", this.recette.getLibelle());
	}

	/**
	 * 
	 */
	public void saveIngredient() {

		if (!listeIngredientsRecette.contains(this.selectedIngredient)) {

			this.ig.setRecette(this.recette);
			this.ig.setIngredient(this.selectedIngredient);

			this.communFacade.saveOrUpdateIngredientRecette(this.ig);

			this.listeIngredientsRecette = this.communFacade.findIngredientByRecette(idRecette);

			this.selectedIngredient = new IngredientPOJO();

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Sauvegarde effectue de l'ingredient  ", " "));
		} else {

			throw new ValidationException(new FeedBackExceptionDTO("erreur "));

		}

	}

	/**
	 * 
	 * @param event
	 */
	public void initDialogIngredient(final CloseEvent event) {

		this.selectedIngredient = null;

		resetInputValues(event.getComponent().getChildren());

	}

	/**
	 * @param event AjaxBehaviorEvent
	 */
	public void openDialogIngredient(final AjaxBehaviorEvent event) {

		selectedIngredient = new IngredientPOJO();
	}

//	public void removeCommune(final LocalitePOJO localite) {
//
//		this.listeLocaliteModifie.add(localite);
//		localite.getListeSecteurs().remove(this.secteur);
//		this.listeLocalites.remove(localite);
//	}

}
