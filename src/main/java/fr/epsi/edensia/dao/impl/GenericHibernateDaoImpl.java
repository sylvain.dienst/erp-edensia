
package fr.epsi.edensia.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Hibernate;
import org.hibernate.Session;

import com.javaetmoi.core.persistence.hibernate.JpaLazyLoadingUtil;

import fr.epsi.edensia.dao.api.IGenericDAO;

/**
 * A generic Hibernate Data Access Object. This implementation assumes that
 * transactions are being handled by the services layer making use of this DAO.
 * The DAO makes use of the Hibernate internal thread local session management
 * via spring.
 * 
 * @author lincolns, wallacew
 * @param <T>
 * @param <PK>
 */
public class GenericHibernateDaoImpl<T, PK extends Serializable> implements IGenericDAO<T, PK> {

	private Class<T> persistentClass;

	@PersistenceContext(unitName = "entityManagerFactory")
	protected EntityManager em;

	/**
	* 
	*/
	@SuppressWarnings({ "unchecked" })
	public GenericHibernateDaoImpl() {
		try {
			persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
					.getActualTypeArguments()[0];
		} catch (final ClassCastException e) {
			// can be raised when DAO is inherited twice
			persistentClass = (Class<T>) ((ParameterizedType) getClass().getSuperclass().getGenericSuperclass())
					.getActualTypeArguments()[0];
		}
	}

	/**
	 * @param id   Long
	 * @param lock boolean
	 * @return T findById
	 */
	public T findById(final Long id, final boolean lock) {
		T entity;
		if (lock) {
			entity = em.find(persistentClass, id, LockModeType.PESSIMISTIC_WRITE);
		} else {
			entity = em.find(persistentClass, id);
		}

		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {
		return em.createQuery("from " + persistentClass.getName()).getResultList();
	}

	@Override
	public List<T> findAll(final List<String[]> ordres) {

		CriteriaQuery<T> criteria = em.getCriteriaBuilder().createQuery(persistentClass);

		Root<T> root = criteria.from(persistentClass);

		if (ordres != null) {
			for (String[] current : ordres) {
				if ("asc".equals(current[0])) {
					criteria.orderBy(em.getCriteriaBuilder().asc(root.get(current[1])));
				} else if ("desc".equals(current[0])) {
					criteria.orderBy(em.getCriteriaBuilder().desc(root.get(current[1])));

				}
			}
		}

		return em.createQuery(criteria).getResultList();
		//
		// return em.createQuery("from " + persistentClass.getName())
		// .getResultList();
	}

	@Override
	public PK save(final T newInstance) {
		em.persist(newInstance);

		return null;
	}

	@Override
	public void saveOrUpdate(final T transientObject) {
		Session session = em.unwrap(Session.class);
		session.saveOrUpdate(transientObject);

	}

	@Override
	public T get(final PK id) {
		// return em.find(persistentClass, id);
		T object = em.find(persistentClass, id);
		if (object == null) {
			throw new NoResultException("No result find : " + persistentClass + " id : " + id);
		}

		return object;
	}

	@Override
	public T getEager(final PK id) {

		T object = em.find(persistentClass, id);
		return JpaLazyLoadingUtil.deepHydrate(em, object);
	}

	@Override
	public T get(final PK id, final boolean returnNullIfEmptyResult) {

		if (!returnNullIfEmptyResult) {
			return get(id);
		} else {
			return em.find(persistentClass, id);
		}
	}

	@Override
	public void update(final T transientObject) {
		em.refresh(transientObject);
	}

	@Override
	public void delete(final T persistentObject) {
		if (!em.contains(persistentObject)) {
			em.remove(em.merge(persistentObject));
		} else {
			em.remove(persistentObject);
		}
	}

	@Override
	public void refresh(final T persistentObject) {
		em.refresh(persistentObject);
	}

	@Override
	public void flush() {
		em.flush();
	}

	@Override
	public Class<T> getPersistentClass() {
		return this.persistentClass;
	}

	@Override
	public void commitTransaction() {
		em.getTransaction().commit();
		em.getTransaction().begin();
	}

	@Override
	public void initialize(final List<T> transientListObject) {
		Hibernate.initialize(transientListObject);
	}

	@Override
	public T merge(final T entity) {
		return em.merge(entity);
	}

}
