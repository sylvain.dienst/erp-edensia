package fr.epsi.edensia.dao.api;

import fr.epsi.edensia.domaine.RecettePOJO;

public interface IRecetteDAO extends IGenericDAO<RecettePOJO, Integer> {

}
