package fr.epsi.edensia.exception;

import fr.epsi.edensia.dto.FeedBackExceptionDTO;

public class DuplicateCodeException extends ValidationException {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @param feedBack
	 *            FeedBackExceptionDTO
	 */
	public DuplicateCodeException(final FeedBackExceptionDTO feedBack) {
		super(feedBack);

	}

	/**
	 * 
	 * @param feedBack
	 *            FeedBackExceptionDTO
	 * @param throwable
	 *            Throwable
	 */
	public DuplicateCodeException(final FeedBackExceptionDTO feedBack,
			final Throwable throwable) {
		super(feedBack, throwable);
	}

}
