
package fr.epsi.edensia.exception;

import fr.epsi.edensia.dto.FeedBackExceptionDTO;




public class ValidationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private FeedBackExceptionDTO feedBack = null;

    /**
     * Constructeur.
     * 
     * @param feedBack
     *            FeedBackExceptionDTO
     */
    public ValidationException(final FeedBackExceptionDTO feedBack) {
	super();
	this.feedBack = feedBack;
    }

    /**
     * Constructeur.
     * 
     * @param feedBack
     *            FeedBackExceptionDTO
     * @param throwable
     *            Throwable
     */
    public ValidationException(final FeedBackExceptionDTO feedBack,
	    final Throwable throwable) {
	super(throwable);
	this.feedBack = feedBack;
    }

    @Override
    public String getMessage() {

	if (feedBack == null) {

	    super.getMessage();
	}

	StringBuilder messages = new StringBuilder();

	for (String current : feedBack.getListMessages()) {

	    if (messages != null && !messages.toString().isEmpty()) {
		messages.append(", ");
	    }
	    messages.append(current);

	}

	return messages.toString();
    }

    /**
     * @return
     */
    public String getMessageRetourLigne() {

	if (feedBack == null) {

	    super.getMessage();
	}

	StringBuilder messages = new StringBuilder();

	for (String current : feedBack.getListMessages()) {

	    if (messages != null && !messages.toString().isEmpty()) {
		messages.append("\n\r");
	    }
	    messages.append(current);

	}

	return messages.toString();
    }

    /**
     * accesseur.
     * 
     * @return the feedBack FeedBackDTO
     */
    public FeedBackExceptionDTO getFeedBack() {
	return feedBack;
    }

    /**
     * accesseur.
     * 
     * @param feedBack
     *            the feedBack to set
     */
    public void setFeedBack(final FeedBackExceptionDTO feedBack) {
	this.feedBack = feedBack;
    }

}
