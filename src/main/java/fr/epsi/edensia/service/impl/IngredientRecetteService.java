package fr.epsi.edensia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.edensia.dao.api.IIngredientRecetteDAO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;
import fr.epsi.edensia.service.api.IIngredientRecetteService;

@Service("ingredientRecetteService")
public class IngredientRecetteService implements IIngredientRecetteService {
	
	//INJECTION
	@Autowired
	private IIngredientRecetteDAO ingredientRecetteDAO;
	//INJECTION
	
	@Override
	public List<IngredientRecettePOJO> findAllIngredientRecette(){
		return this.ingredientRecetteDAO.findAll();
	}
	
	@Override
	public IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette) {
		return this.ingredientRecetteDAO.get(idIngredientRecette);
	}
	
	@Override
	public void saveIngredientRecette (IngredientRecettePOJO ingredientRecette) {
		this.ingredientRecetteDAO.saveOrUpdate(ingredientRecette);
	}
	
	@Override
	public void removeIngredientRecette (IngredientRecettePOJO ingredientRecette) {
		this.ingredientRecetteDAO.delete(ingredientRecette);
	}
	
	@Override
	public List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette){
		return this.ingredientRecetteDAO.findlisteIngredientsByRecette(idRecette);
	}

}
