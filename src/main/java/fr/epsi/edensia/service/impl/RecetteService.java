package fr.epsi.edensia.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.epsi.edensia.dao.api.IRecetteDAO;
import fr.epsi.edensia.domaine.RecettePOJO;
import fr.epsi.edensia.service.api.IRecetteService;

@Service("recetteService")
public class RecetteService implements IRecetteService {
	
	//INJECTION
	@Autowired
	private IRecetteDAO recetteDAO;
	//INJECTION

	
	@Override
	public List<RecettePOJO> findAllRecette() {
		return this.recetteDAO.findAll();
	}
	
	@Override
	public RecettePOJO getRecette(Integer idRecette) {
		return this.recetteDAO.get(idRecette);
	}
	
	@Override
	public void saveRecette(RecettePOJO recette) {
		this.recetteDAO.saveOrUpdate(recette);
	}
	
	@Override
	public void removeRecette(RecettePOJO recette) {
		this.recetteDAO.delete(recette);
	}

}
