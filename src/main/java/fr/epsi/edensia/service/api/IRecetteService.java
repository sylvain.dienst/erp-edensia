package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.RecettePOJO;

public interface IRecetteService {

	/**
	 * 
	 * @param recette
	 */
	void removeRecette(RecettePOJO recette);

	/**
	 * 
	 * @param recette
	 */
	void saveRecette(RecettePOJO recette);

	/**
	 * 
	 * @param idRecette
	 * @return
	 */
	RecettePOJO getRecette(Integer idRecette);

	/**
	 * 
	 * @return
	 */
	List<RecettePOJO> findAllRecette();

}
