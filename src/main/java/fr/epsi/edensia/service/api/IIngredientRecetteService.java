package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientRecettePOJO;

public interface IIngredientRecetteService {

	/**
	 * 
	 * @return
	 */
	List<IngredientRecettePOJO> findAllIngredientRecette();

	/**
	 * 
	 * @param idIngredientRecette
	 * @return
	 */
	IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette);

	/**
	 * 
	 * @param ingredientRecette
	 */
	void saveIngredientRecette(IngredientRecettePOJO ingredientRecette);

	/**
	 * 
	 * @param ingredientRecette
	 */
	void removeIngredientRecette(IngredientRecettePOJO ingredientRecette);

	/**
	 * 
	 * @param idRecette
	 * @return
	 */
	List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette);

}
