package fr.epsi.edensia.service.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientPOJO;

public interface IIngredientService {

	/**
	 * 
	 * @return
	 */
	List<IngredientPOJO> findAllIngredient();

	/**
	 * 
	 * @param idIngredient
	 * @return
	 */
	IngredientPOJO getIngredient(Integer idIngredient);

	/**
	 * 
	 * @param ingredient
	 */
	void saveIngredient(IngredientPOJO ingredient);

	/**
	 * 
	 * @param ingredient
	 */
	void removeIngredient(IngredientPOJO ingredient);

}
