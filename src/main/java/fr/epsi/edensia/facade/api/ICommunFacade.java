package fr.epsi.edensia.facade.api;

import java.util.List;

import fr.epsi.edensia.domaine.IngredientPOJO;
import fr.epsi.edensia.domaine.IngredientRecettePOJO;
import fr.epsi.edensia.domaine.RecettePOJO;

public interface ICommunFacade {

	/**
	 * 
	 * @return
	 */
	List<RecettePOJO> findAllRecette();

	/**
	 * 
	 * @param idRecette
	 * @return
	 */
	RecettePOJO getRecette(Integer idRecette);

	/**
	 * 
	 * @param recette
	 */
	void saveOrUpdateRecette(RecettePOJO recette);

	/**
	 * 
	 * @param recette
	 */
	void removeRecette(RecettePOJO recette);

	/**
	 * 
	 * @return
	 */
	List<IngredientPOJO> findAllIngredient();

	/**
	 * 
	 * @param idIngredient
	 * @return
	 */
	IngredientPOJO findIngredient(Integer idIngredient);

	/**
	 * 
	 * @param ingredient
	 */
	void saveOrUpdateIngredient(IngredientPOJO ingredient);

	/**
	 * 
	 * @param ingredient
	 */
	void removeIngredient(IngredientPOJO ingredient);

	/**
	 * 
	 * @return
	 */
	List<IngredientRecettePOJO> findAllIngredientRecette();

	/**
	 * 
	 * @param idIngredientRecette
	 * @return
	 */
	IngredientRecettePOJO getIngredientRecette(Integer idIngredientRecette);

	/**
	 * 
	 * @param ingredientRecette
	 */
	void saveOrUpdateIngredientRecette(IngredientRecettePOJO ingredientRecette);

	/**
	 * 
	 * @param ingredientRecette
	 */
	void removeIngredientRecette(IngredientRecettePOJO ingredientRecette);

	/**
	 * 
	 * @param idRecette
	 * @return
	 */
	List<IngredientRecettePOJO> findIngredientByRecette(Integer idRecette);

}
