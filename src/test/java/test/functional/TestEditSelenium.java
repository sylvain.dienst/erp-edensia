//// Generated by Selenium IDE
//
//package test.functional;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "/applicationContext-test.xml" })
//public class TestEditSelenium {
//	private WebDriver driver;
//	private Map<String, Object> vars;
//	JavascriptExecutor js;
//
//	public void testEditSelenium() {
//		driver = new ChromeDriver();
//		js = (JavascriptExecutor) driver;
//		vars = new HashMap<String, Object>();
//
//		driver.get("http://localhost:8080/edensia/");
//		driver.manage().window().setSize(new Dimension(1440, 797));
//		driver.findElement(By.cssSelector("td:nth-child(2)")).click();
//		driver.findElement(By.cssSelector("#boutton_consult > .ui-button-text")).click();
//		driver.findElement(By.cssSelector("#form_recette\\3A boutton_modify > .ui-button-text")).click();
//		driver.findElement(By.cssSelector(".ui-state-hover .ui-icon")).click();
//		driver.findElement(By.cssSelector("#form_recette\\3A boutton_save > .ui-button-text")).click();
//
//		driver.quit();
//	}
//}
